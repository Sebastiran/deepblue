﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class OxygenBar : MonoBehaviour
{
    public RectTransform filler;
    public Image overlay;
    public TMP_Text statusText;

    private void Update()
    {
        Vector2 newAnchoredPosition = new Vector2(0, 0);
        newAnchoredPosition.y = Mathf.Lerp(0, -filler.sizeDelta.y, (PlayerStats.Instance.MaxOxygen - PlayerStats.Instance.Oxygen) / PlayerStats.Instance.MaxOxygen);
        filler.anchoredPosition = newAnchoredPosition;
        statusText.text = PlayerStats.Instance.Oxygen.ToString("0");
    }
}
