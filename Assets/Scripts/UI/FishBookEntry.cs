﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FishBookEntry : MonoBehaviour
{
    public string _name = "";
    public Image image;
    public TMP_Text fishName;
    public TMP_Text value;
    public TMP_Text amount;

    private PlayerPrefsManager ppm;

    private void Awake()
    {
        ppm = PlayerPrefsManager.Instance;
    }

    private void OnEnable()
    {
        UpdateEntry();
    }

    public void UpdateEntry()
    {
        if (ppm.FishFound(_name))
        {
            fishName.text = _name;
        }
        else
        {
            fishName.text = "???";
        }
        if (ppm.FishCaught(_name))
        {
            value.text = "$" + GameManager.Instance.GetResourceByName(_name).value.ToString();
            amount.text = "Caught: " + ppm.FishAmount(_name).ToString();
        }
        else
        {
            value.text = "$???";
            amount.text = "Caught: 0";
        }
    }
}
