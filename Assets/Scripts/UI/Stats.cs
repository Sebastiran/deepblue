﻿using UnityEngine;

public class Stats : MonoBehaviour
{
    private PlayerStats stats;
    public Transform player;

    private void Awake()
    {
        stats = PlayerStats.Instance;
    }

    private void OnGUI()
    {
        GUIStyle style = new GUIStyle();
        style.normal.textColor = Color.white;
        style.fontSize = 16;

        GUI.Label(new Rect(5, 5, 150, 100), "Oxygen: " + stats.Oxygen.ToString("0"), style);
        GUI.Label(new Rect(5, 25, 150, 100), "Cargo: " + stats.CurrentCargo + "/" + stats.MaxCargo, style);
        GUI.Label(new Rect(5, 45, 150, 100), "Money: " + stats.Money, style);
        GUI.Label(new Rect(5, 65, 150, 100), "Depth: " + stats.Depth.ToString("0") + "/" + stats.MaxDepth, style);
        GUI.Label(new Rect(5, 85, 150, 100), "Health: " + stats.Health.ToString("0"), style);
    }
}
