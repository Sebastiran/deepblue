﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DepthBar : MonoBehaviour
{
    public RectTransform filler;
    public Image overlay;
    public TMP_Text statusText;

    private void Update()
    {
        Vector2 newAnchoredPosition = new Vector2(0, 0);
        newAnchoredPosition.y = Mathf.Lerp(0, -filler.sizeDelta.y, (PlayerStats.Instance.MaxDepth - PlayerStats.Instance.Depth) / PlayerStats.Instance.MaxDepth);
        filler.anchoredPosition = newAnchoredPosition;
        statusText.text = PlayerStats.Instance.Depth.ToString("0");
    }
}
