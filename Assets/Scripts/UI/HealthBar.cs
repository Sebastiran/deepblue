﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HealthBar : MonoBehaviour
{
    public RectTransform filler;
    public Image overlay;
    public TMP_Text statusText;

    private void Awake()
    {
        UpdateBar();
    }

    private void UpdateBar()
    {
        Vector2 newAnchoredPosition = new Vector2(0,0);
        newAnchoredPosition.y = Mathf.Lerp(0, -filler.sizeDelta.y, (PlayerStats.Instance.MaxHealth - PlayerStats.Instance.Health) / PlayerStats.Instance.MaxHealth);
        filler.anchoredPosition = newAnchoredPosition;
        statusText.text = PlayerStats.Instance.Health.ToString("0");
    }

    private void OnEnable()
    {
        PlayerStats.discreteStatChanged += UpdateBar;
    }

    private void OnDisable()
    {
        PlayerStats.discreteStatChanged -= UpdateBar;
    }
}
