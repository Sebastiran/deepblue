﻿using System.Collections.Generic;
using UnityEngine;

public class WindowManager : Instancer<WindowManager>
{
    public enum Window
    {
        Seller = 0,
        Shop = 1,
        Repair = 2,
        FishBook = 3
    }

    public GenericWindow[] windows;
    public int currentWindowID;
    public int defaultWindowID;
    public int masterWindowID = -1;
    public List<GenericWindow> openWindows = new List<GenericWindow>();

    void Start()
    {
        OpenOnly(defaultWindowID);
    }

    /** Close all windows*/
    public void CloseAll()
    {
        openWindows.Clear();
        currentWindowID = masterWindowID;

        var total = windows.Length;

        for (var i = 0; i < total; i++)
        {
            var window = windows[i];
            if (window.gameObject.activeSelf)
            {
                window.CloseInstant();
            }
        }
        Open(masterWindowID);
    }

    /** Close this window if open*/
    public void Close(int value)
    {
        if (value < 0 || value >= windows.Length)
            return;

        openWindows[value].Close();
        openWindows.Remove(openWindows[value]);
        if (openWindows.Count <= 0)
        {
            Open(masterWindowID);
        }
    }

    /** Open a window and hide all other open windows*/
    public void Open(int value)
    {
        if (value < 0 || value >= windows.Length)
            return;

        openWindows.Add(windows[value]);
        ToggleVisability(value);
    }

    /** Open an extra window next to the windows already open*/
    public void OpenExtra(int value)
    {
        if (value < 0 || value >= windows.Length)
            return;

        openWindows.Add(windows[value]);
        windows[value].Open();
        OrderOpenWindows();
    }

    /** Open a window and close all other windows*/
    public void OpenOnly(int value)
    {
        CloseAll();
        if (value != masterWindowID)
            Open(value);
    }

    /** Replace the currenct top level window*/
    public void ReplaceCurrenct(int value)
    {
        Pop();
        Open(value);
    }

    /** Close the top level window*/
    public void Pop()
    {
        if (openWindows.Count > 0)
        {
            Close(openWindows.Count - 1);
            if (openWindows.Count > 0)
            {
                if (!openWindows[openWindows.Count - 1].gameObject.activeSelf)
                    ToggleVisability(GetWindowIndex(openWindows[openWindows.Count - 1]));
            }
        }
    }

    /** Close this window if it is on top*/
    public void PopIfOnTop(int value)
    {
        if (GetWindowIndex(openWindows[openWindows.Count - 1]) == value)
        {
            Pop();
        }
    }

    public bool IsOpen(int value)
    {
        bool open = false;
        for(int i = 0; i < openWindows.Count; i++)
        {
            if (GetWindowIndex(openWindows[i]) == value)
            {
                open = true;
            }
        }
        return open;
    }

    /** Get a window by its index*/
    public GenericWindow GetWindow(int value)
    {
        return windows[value];
    }
    /** Get a window by its gameobject name*/
    public GenericWindow GetWindow(string name)
    {
        foreach (GenericWindow w in windows)
        {
            if (w.gameObject.name == name)
                return w;
        }
        return null;
    }
    /** Get the index of a window in the windows array*/
    public int GetWindowIndex(GenericWindow window)
    {
        for (int i = 0; i < windows.Length; i++)
        {
            if (windows[i] == window)
                return i;
        }
        return -1;
    }

    /** Hide all windows but this one*/
    private void ToggleVisability(int value)
    {
        currentWindowID = value;

        var total = windows.Length;

        var windowToOpen = windows[value];

        float openAfter = 0f;

        for (var i = 0; i < total; i++)
        {
            var window = windows[i];
            if (i != value && window.gameObject.activeSelf)
            {
                openAfter = window.Close();
            }
        }
        windowToOpen.OpenAfter(openAfter);
    }

    /** Set the rendering order of the open windows*/
    private void OrderOpenWindows()
    {
        for (int i = 0; i < openWindows.Count; i++)
        {
            openWindows[i].GetComponent<Canvas>().sortingOrder = i + 1;
        }
    }
}