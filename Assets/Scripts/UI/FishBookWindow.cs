﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FishBookWindow : GenericWindow
{
    public override void Cancel()
    {
        GameManager.Instance.Pause = false;
        base.Cancel();
    }
}
