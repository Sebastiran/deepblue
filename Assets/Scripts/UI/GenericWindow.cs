﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Canvas))]
[RequireComponent(typeof(GraphicRaycaster))]
[RequireComponent(typeof(CanvasGroup))]
public class GenericWindow : MonoBehaviour
{
    public GameObject[] panels;

    protected virtual void Display(bool value)
    {
        gameObject.SetActive(value);
    }

    public virtual void Open()
    {
        GetComponent<CanvasGroup>().alpha = 1;
        for (int i = 0; i < panels.Length; i++)
        {
            panels[i].GetComponent<Animator>().SetTrigger("Open");
        }
    }

    public virtual void OpenAfter(float time)
    {
        Display(true);
        GetComponent<CanvasGroup>().alpha = 0;
        Invoke("Open", time);
    }

    public virtual void OpenInstant()
    {
        Display(true);
    }

    public virtual float Close()
    {
        float animationLength = 0f;
        for (int i = 0; i < panels.Length; i++)
        {
            panels[i].GetComponent<Animator>().SetTrigger("Close");
            if (panels[i].GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length > animationLength)
            {
                animationLength = panels[i].GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length;
            }
        }
        Invoke("CloseInstant", animationLength);
        return animationLength;
    }

    public virtual void CloseInstant()
    {
        Display(false);
    }

    public virtual void Cancel()
    {
        WindowManager.Instance.Pop();
    }
}