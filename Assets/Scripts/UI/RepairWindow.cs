﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections.Generic;
using TMPro;

public class RepairWindow : GenericWindow
{
    private PlayerStats stats;

    [SerializeField]
    private TMP_Text healthStatus;
    [SerializeField]
    private GameObject repairAllButton;
    [SerializeField]
    private GameObject repairTenButton;

    private float pricePerHealth = 2f;

    private void Awake()
    {
        stats = PlayerStats.Instance;
    }

    private void OnEnable()
    {
        UpdatePanel();
    }

    private void UpdatePanel()
    {
        healthStatus.text = "Health: " + stats.Health;

        TMP_Text btnText = repairAllButton.GetComponentInChildren<TMP_Text>();
        btnText.text = "Repair All: $" + (int)((stats.MaxHealth - stats.Health) * pricePerHealth);

        if (stats.Money < (stats.MaxHealth - stats.Health) * pricePerHealth)
        {
            repairAllButton.GetComponent<Button>().interactable = false;
        }
        else
        {
            repairAllButton.GetComponent<Button>().interactable = true;
        }

        TMP_Text btnTenText = repairTenButton.GetComponentInChildren<TMP_Text>();
        btnTenText.text = "Repair 10: $" + (int)(10 * pricePerHealth);

        if (stats.Money < 10 * pricePerHealth || stats.MaxHealth - stats.Health < 10)
        {
            repairTenButton.GetComponent<Button>().interactable = false;
        }
        else
        {
            repairTenButton.GetComponent<Button>().interactable = true;
        }
    }

    public void RepairTen()
    {
        if (stats.Money >= 10 * pricePerHealth)
        {
            stats.Money -= (int)(10 * pricePerHealth);
            stats.Health += 10;
            UpdatePanel();
        }
    }

    public void RepairAll()
    {
        if (stats.Money >= (stats.MaxHealth - stats.Health) * pricePerHealth)
        {
            stats.Money -= (int)((stats.MaxHealth - stats.Health) * pricePerHealth);
            stats.Health = stats.MaxHealth;
            UpdatePanel();
            Cancel();
        }
    }

    public override void Cancel()
    {
        GameManager.Instance.Pause = false;
        base.Cancel();
    }
}
