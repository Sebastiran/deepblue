﻿using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using UnityEngine;

public class SellWindow : GenericWindow
{
    private PlayerStats stats;
    public TMP_Text body;

    public struct ResourceList
    {
        public string name;
        public int amount;
        public float value;

        public ResourceList(string name, int amount, float value)
        {
            this.name = name;
            this.amount = amount;
            this.value = value;
        }
    }

    private float totalValue = 0f;

    private void Awake()
    {
        stats = PlayerStats.Instance;
    }

    private void OnEnable()
    {
        InitTabel();
    }

    private void InitTabel()
    {
        body.text = "";

        List<List<Resource>> resources = new List<List<Resource>>();
        for (int i = 0; i < stats.resources.Count; i++)
        {
            bool exists = false;
            for (int j = 0; j < resources.Count; j++)
            {
                if (stats.resources[i]._name == resources[j][0]._name)
                {
                    resources[j].Add(stats.resources[i]);
                    exists = true;
                }
            }
            if (!exists)
            {
                List<Resource> newResourceList = new List<Resource>();
                newResourceList.Add(stats.resources[i]);
                resources.Add(newResourceList);
            }
        }

        for (int i = 0; i < resources.Count; i++)
        {
            float value = resources[i].Count * resources[i][0].value;
            body.text += resources[i][0]._name + "\t" + resources[i][0].value + "\t\t" + resources[i].Count + "\t\t" + value + "\n";
            totalValue += value;
        }
    }

    public void Sell()
    {
        stats.Money += totalValue;
        totalValue = 0f;
        stats.CurrentCargo = 0f;
        stats.resources.Clear();
        Cancel();
    }

    public override void Cancel()
    {
        GameManager.Instance.Pause = false;
        base.Cancel();
    }
}
