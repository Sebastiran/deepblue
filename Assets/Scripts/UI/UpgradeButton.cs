﻿using UnityEngine;
using UnityEngine.UI;

public class UpgradeButton : MonoBehaviour
{
    public ShopWindow shop;
    private UpgradeManager um;

    public string _name = "";
    private IUpgrade upgrade;
    private Button button;
    private Image image;

    private void Awake()
    {
        um = GameManager.Instance.GetComponent<UpgradeManager>();
        upgrade = um.GetUpgradeByName(_name);
        button = GetComponent<Button>();
        image = GetComponent<Image>();

        button.onClick.AddListener(SelectUpgrade);
    }

    public void SelectUpgrade()
    {
        shop.SelectUpgrade(_name);
    }

    public void UpdateButton()
    {
        upgrade = um.GetUpgradeByName(_name);
        if (upgrade.Enabled)
        {
            image.color = Color.cyan;
        }
        else if (upgrade.Unlocked)
        {
            image.color = Color.white;
        }
        else if (um.IsUpgradeUnlockable(upgrade))
        {
            if (upgrade.Price <= PlayerStats.Instance.Money)
            {
                image.color = Color.green;
            }
            else
            {
                image.color = Color.yellow;
            }
        }
        else
        {
            image.color = Color.red;
        }
    }
}
