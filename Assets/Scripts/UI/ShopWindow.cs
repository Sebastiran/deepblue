﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections.Generic;
using TMPro;

public class ShopWindow : GenericWindow
{
    private UpgradeManager um;
    private PlayerStats stats;

    [SerializeField]
    private GameObject buyButton;
    [SerializeField]
    private TMP_Text flavourText;
    [SerializeField]
    private GameObject[] atributes;

    private IUpgrade selectedUpgrade;

    private void Awake()
    {
        stats = PlayerStats.Instance;
        um = GameManager.Instance.GetComponent<UpgradeManager>();
    }

    private void OnEnable()
    {
        UpdateShop();
    }

    private void UpdateShop()
    {
        Text buyText = buyButton.GetComponentInChildren<Text>();
        Button BButton = buyButton.GetComponent<Button>();
        if (selectedUpgrade == null)
        {
            buyButton.gameObject.SetActive(false);
            flavourText.text = "";
        }
        else
        {
            buyButton.SetActive(true);
            BButton.interactable = true;

            if (!selectedUpgrade.Unlocked)
            {
                if (!um.IsUpgradeUnlockable(selectedUpgrade))
                    BButton.interactable = false;

                buyText.text = "$" + selectedUpgrade.Price;
            }
            else if (!selectedUpgrade.Enabled)
            {
                buyText.text = "Equip";
            }
            else
            {
                buyText.text = "Unequip";
                //BButton.interactable = false;
            }

            flavourText.text = selectedUpgrade.Flavour;
        }

        for (int i = 0; i < atributes.Length; i++)
            atributes[i].GetComponent<UpgradeButton>().UpdateButton();
    }

    public void SelectUpgrade(string name)
    {
        selectedUpgrade = um.GetUpgradeByName(name);
        UpdateShop();
    }

    public void BuyUpgrade()
    {
        IUpgrade upgrade = selectedUpgrade;
        if (upgrade.Unlocked)
        {
            if (upgrade.Enabled)
                UnequipUpgrade(upgrade);
            else
                EquipUpgrade(upgrade);
        }
        else if (stats.Money >= upgrade.Price)
        {
            stats.Money -= upgrade.Price;
            upgrade.Unlocked = true;
            EquipUpgrade(upgrade);
        }
    }

    public void EquipUpgrade(string name)
    {
        EquipUpgrade(um.GetUpgradeByName(name));
    }
    public void EquipUpgrade(IUpgrade upgrade)
    {
        um.Apply(upgrade);
        UpdateShop();
    }

    private void UnequipUpgrade(IUpgrade upgrade)
    {
        um.Unapply(upgrade);
        UpdateShop();
    }

    public override void Cancel()
    {
        GameManager.Instance.Pause = false;
        base.Cancel();
    }
}
