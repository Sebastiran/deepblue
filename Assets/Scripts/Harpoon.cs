﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class Harpoon : MonoBehaviour
{
    public GameObject player;
    private Submarine submarine;
    private Vector3 direction;
    public float maxDistance = 6f;
    private float currentMaxDistance = 6f;
    private float defaultRetrieveSpeed = 5f;
    private float retrieveSpeed = 5f;

    public LineRenderer line;
    private SpriteRenderer _spriteRenderer;
    private Rigidbody2D _rigidBody;

    private List<GameObject> resources = new List<GameObject>();
    private CameraShake camShake;

    public enum State
    {
        idle = 0,
        casting = 1,
        retrieving = 2,
        locked = 3
    }

    public static int state = (int)State.idle;

    private void Awake()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
        _rigidBody.simulated = false;
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _spriteRenderer.enabled = false;
        submarine = player.GetComponent<Submarine>();
        camShake = GameManager.Instance.GetComponent<CameraShake>();
        line.enabled = false;
    }

    public void Clear()
    {
        resources.Clear();
        ResetLine();
    }

    private void ResetLine()
    {
        state = (int)State.idle;
        currentMaxDistance = maxDistance;
        _spriteRenderer.enabled = false;
        _rigidBody.simulated = false;
        retrieveSpeed = defaultRetrieveSpeed;

        line.enabled = false;
    }

    public void Shoot()
    {
        if (state == (int)State.idle && GameManager.Instance.allowControl)
        {
            state = (int)State.casting;
            AudioManager.Instance.PlaySound("Shoot");
            _spriteRenderer.enabled = true;
            _rigidBody.simulated = true;
            transform.position = player.transform.position;

            Vector3 targetPos = GameManager.Instance.GetWorldPositionOnPlane(Input.mousePosition, 0f);
            targetPos.z = 0;
            direction = (targetPos - player.transform.position).normalized;

            line.enabled = true;
            line.SetPosition(0, player.transform.position);
            line.SetPosition(1, transform.position);
        }
    }

    private void Update()
    {
        if (line.enabled)
        {
            line.SetPosition(0, player.transform.position);
            line.SetPosition(1, transform.position);
        }

        if (state == (int)State.casting)
        {
            _rigidBody.AddForce(direction * 1f, ForceMode2D.Impulse);
            if (Vector2.Distance(transform.position, player.transform.position) > maxDistance)
            {
                state = (int)State.retrieving;
            }
        }

        if (state == (int)State.retrieving)
        {
            if (Vector2.Distance(transform.position, player.transform.position) < 1f)
            {
                Collect();
                ResetLine();
            }

            currentMaxDistance -= retrieveSpeed * Time.deltaTime;
            if (Vector2.Distance(transform.position, player.transform.position) > currentMaxDistance)
            {
                transform.position = player.transform.position + (transform.position - player.transform.position).normalized * currentMaxDistance;
            }
        }

        if (state == (int)State.locked)
        {
            if (Vector2.Distance(transform.position, player.transform.position) > maxDistance)
            {
                // Shake the camera if the whale is pulling on the rope
                if (!camShake.shaking && resources[0].GetComponent<Resource>()._name == "Whale")
                {
                    camShake.Shake(0.1f, 0.1f);
                }
                player.transform.position = transform.position + (player.transform.position - transform.position).normalized * maxDistance;
            }

            bool dead = true;
            foreach (GameObject g in resources)
            {
                if (g.GetComponent<Resource>().health > 0)
                {
                    dead = false;
                }
            }
            if (dead)
            {
                state = (int)State.retrieving;
                retrieveSpeed = retrieveSpeed / 2f;
            }
        }

        foreach (GameObject g in resources)
        {
            if (g.GetComponent<Resource>().bigFish)
            {
                transform.position = g.transform.position;
            }
            g.transform.localPosition = Vector3.zero;
        }
    }

    private void Attach(GameObject resource)
    {
        resource.transform.parent = transform;
        resource.transform.localPosition = Vector3.zero;
        if (!resource.GetComponent<Resource>().attached)
        {
            if (resource.GetComponent<Resource>().bigFish)
            {
                Collect();
                state = (int)State.locked;
            }

            resource.GetComponent<Resource>().Hit();
            resources.Add(resource);
        }
    }

    private void Collect()
    {
        foreach (GameObject g in resources)
        {
            submarine.Collect(g.GetComponent<Resource>());
            g.SetActive(false);
        }
        resources.Clear();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Resource" && state == (int)State.casting)
        {
            Attach(collision.gameObject);
        }
    }
}
