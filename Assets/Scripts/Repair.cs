﻿using UnityEngine;

public class Repair : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            if (!WindowManager.Instance.IsOpen((int)WindowManager.Window.Repair) && SubmarineMovement.water != (int)SubmarineMovement.Water.inWater)
            {
                GameManager.Instance.Pause = true;
                WindowManager.Instance.Open((int)WindowManager.Window.Repair);
            }
        }
    }
}
