﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUpgrade
{
    string Name { get; }
    string AppliesTo { get; }
    object Amount { get; }
    float Price { get; }
    bool Enabled { get; set; }
    bool Unlocked { get; set; }
    string Flavour { get; }
    string UpgradeGroup { get; }
    int IndexInGroup { get; }

    void Apply();
    void Unapply();
}

public class OxygenTier1 : MonoBehaviour, IUpgrade
{
    string _name = "Oxygen Tier 1";
    string _appliesTo = "maxOxygen";
    object _amount = 100f;
    float _price = 500f;
    bool _enabled = false;
    bool _unlocked = false;
    string _flavour = "A brand new oxygen tank to keep you oxidated underwater.";
    string _upgradeGroup = "oxygen";
    int _indexInGroup = 0;

    public string Name
    {
        get
        {
            return _name;
        }
    }
    public object Amount
    {
        get
        {
            return _amount;
        }
    }
    public float Price
    {
        get
        {
            return _price;
        }
    }
    public string AppliesTo
    {
        get
        {
            return _appliesTo;
        }
    }
    public bool Enabled
    {
        get
        {
            return _enabled;
        }

        set
        {
            _enabled = value;
        }
    }
    public bool Unlocked
    {
        get
        {
            return _unlocked;
        }

        set
        {
            _unlocked = value;
        }
    }
    public string Flavour
    {
        get
        {
            return _flavour;
        }
    }
    public string UpgradeGroup
    {
        get
        {
            return _upgradeGroup;
        }
    }
    public int IndexInGroup
    {
        get
        {
            return _indexInGroup;
        }
    }

    public void Apply()
    {
        PlayerStats.Instance.stats[_appliesTo] = PlayerStats.Instance.baseOxygen + (float)_amount;
        PlayerStats.Instance.Oxygen = (float)PlayerStats.Instance.stats[_appliesTo];
        _enabled = true;
    }

    public void Unapply()
    {
        PlayerStats.Instance.stats[_appliesTo] = PlayerStats.Instance.baseOxygen;
        PlayerStats.Instance.Oxygen = (float)PlayerStats.Instance.stats[_appliesTo];
        _enabled = false;
    }
}

public class OxygenTier2 : MonoBehaviour, IUpgrade
{
    string _name = "Oxygen Tier 2";
    string _appliesTo = "maxOxygen";
    object _amount = 300f;
    float _price = 2300f;
    bool _enabled = false;
    bool _unlocked = false;
    string _flavour = "An even bigger oxygen tank.";
    string _upgradeGroup = "oxygen";
    int _indexInGroup = 1;

    public string Name
    {
        get
        {
            return _name;
        }
    }
    public object Amount
    {
        get
        {
            return _amount;
        }
    }
    public float Price
    {
        get
        {
            return _price;
        }
    }
    public string AppliesTo
    {
        get
        {
            return _appliesTo;
        }
    }
    public bool Enabled
    {
        get
        {
            return _enabled;
        }

        set
        {
            _enabled = value;
        }
    }
    public bool Unlocked
    {
        get
        {
            return _unlocked;
        }

        set
        {
            _unlocked = value;
        }
    }
    public string Flavour
    {
        get
        {
            return _flavour;
        }
    }
    public string UpgradeGroup
    {
        get
        {
            return _upgradeGroup;
        }
    }
    public int IndexInGroup
    {
        get
        {
            return _indexInGroup;
        }
    }

    public void Apply()
    {
        PlayerStats.Instance.stats[_appliesTo] = PlayerStats.Instance.baseOxygen + (float)_amount;
        PlayerStats.Instance.Oxygen = (float)PlayerStats.Instance.stats[_appliesTo];
        _enabled = true;
    }

    public void Unapply()
    {
        PlayerStats.Instance.stats[_appliesTo] = PlayerStats.Instance.baseOxygen;
        PlayerStats.Instance.Oxygen = (float)PlayerStats.Instance.stats[_appliesTo];
        _enabled = false;
    }
}

public class CargoTier1 : MonoBehaviour, IUpgrade
{
    string _name = "Cargo Tier 1";
    string _appliesTo = "maxCargo";
    object _amount = 100f;
    float _price = 500f;
    bool _enabled = false;
    bool _unlocked = false;
    string _flavour = "Neat little bin for you to put your fish in.";
    string _upgradeGroup = "cargo";
    int _indexInGroup = 0;

    public string Name
    {
        get
        {
            return _name;
        }
    }
    public object Amount
    {
        get
        {
            return _amount;
        }
    }
    public float Price
    {
        get
        {
            return _price;
        }
    }
    public string AppliesTo
    {
        get
        {
            return _appliesTo;
        }
    }
    public bool Enabled
    {
        get
        {
            return _enabled;
        }

        set
        {
            _enabled = value;
        }
    }
    public bool Unlocked
    {
        get
        {
            return _unlocked;
        }

        set
        {
            _unlocked = value;
        }
    }
    public string Flavour
    {
        get
        {
            return _flavour;
        }
    }
    public string UpgradeGroup
    {
        get
        {
            return _upgradeGroup;
        }
    }
    public int IndexInGroup
    {
        get
        {
            return _indexInGroup;
        }
    }

    public void Apply()
    {
        PlayerStats.Instance.stats[_appliesTo] = PlayerStats.Instance.baseMaxCargo + (float)_amount;
        _enabled = true;
    }

    public void Unapply()
    {
        PlayerStats.Instance.stats[_appliesTo] = PlayerStats.Instance.baseMaxCargo;
        _enabled = false;
    }
}

public class DepthTier1 : MonoBehaviour, IUpgrade
{
    string _name = "Depth Tier 1";
    string _appliesTo = "maxDepth";
    object _amount = 150f;
    float _price = 700f;
    bool _enabled = false;
    bool _unlocked = false;
    string _flavour = "Can get you to 250 depth";
    string _upgradeGroup = "depth";
    int _indexInGroup = 0;

    public string Name
    {
        get
        {
            return _name;
        }
    }
    public object Amount
    {
        get
        {
            return _amount;
        }
    }
    public float Price
    {
        get
        {
            return _price;
        }
    }
    public string AppliesTo
    {
        get
        {
            return _appliesTo;
        }
    }
    public bool Enabled
    {
        get
        {
            return _enabled;
        }

        set
        {
            _enabled = value;
        }
    }
    public bool Unlocked
    {
        get
        {
            return _unlocked;
        }

        set
        {
            _unlocked = value;
        }
    }
    public string Flavour
    {
        get
        {
            return _flavour;
        }
    }
    public string UpgradeGroup
    {
        get
        {
            return _upgradeGroup;
        }
    }
    public int IndexInGroup
    {
        get
        {
            return _indexInGroup;
        }
    }

    public void Apply()
    {
        PlayerStats.Instance.stats[_appliesTo] = PlayerStats.Instance.baseDepth + (float)_amount;
        _enabled = true;
    }

    public void Unapply()
    {
        PlayerStats.Instance.stats[_appliesTo] = PlayerStats.Instance.baseDepth;
        _enabled = false;
    }
}

public class ArmorTier1 : MonoBehaviour, IUpgrade
{
    string _name = "Armor Tier 1";
    string _appliesTo = "armor";
    object _amount = 20f;
    float _price = 600f;
    bool _enabled = false;
    bool _unlocked = false;
    string _flavour = "Take less damage.";
    string _upgradeGroup = "armor";
    int _indexInGroup = 0;

    public string Name
    {
        get
        {
            return _name;
        }
    }
    public object Amount
    {
        get
        {
            return _amount;
        }
    }
    public float Price
    {
        get
        {
            return _price;
        }
    }
    public string AppliesTo
    {
        get
        {
            return _appliesTo;
        }
    }
    public bool Enabled
    {
        get
        {
            return _enabled;
        }

        set
        {
            _enabled = value;
        }
    }
    public bool Unlocked
    {
        get
        {
            return _unlocked;
        }

        set
        {
            _unlocked = value;
        }
    }
    public string Flavour
    {
        get
        {
            return _flavour;
        }
    }
    public string UpgradeGroup
    {
        get
        {
            return _upgradeGroup;
        }
    }
    public int IndexInGroup
    {
        get
        {
            return _indexInGroup;
        }
    }

    public void Apply()
    {
        PlayerStats.Instance.stats[_appliesTo] = PlayerStats.Instance.baseArmor + (float)_amount;
        _enabled = true;
    }

    public void Unapply()
    {
        PlayerStats.Instance.stats[_appliesTo] = PlayerStats.Instance.baseArmor;
        _enabled = false;
    }
}

public class HealthTier1 : MonoBehaviour, IUpgrade
{
    string _name = "Health Tier 1";
    string _appliesTo = "maxHealth";
    object _amount = 50f;
    float _price = 600f;
    bool _enabled = false;
    bool _unlocked = false;
    string _flavour = "Take those hits.";
    string _upgradeGroup = "health";
    int _indexInGroup = 0;

    public string Name
    {
        get
        {
            return _name;
        }
    }
    public object Amount
    {
        get
        {
            return _amount;
        }
    }
    public float Price
    {
        get
        {
            return _price;
        }
    }
    public string AppliesTo
    {
        get
        {
            return _appliesTo;
        }
    }
    public bool Enabled
    {
        get
        {
            return _enabled;
        }

        set
        {
            _enabled = value;
        }
    }
    public bool Unlocked
    {
        get
        {
            return _unlocked;
        }

        set
        {
            _unlocked = value;
        }
    }
    public string Flavour
    {
        get
        {
            return _flavour;
        }
    }
    public string UpgradeGroup
    {
        get
        {
            return _upgradeGroup;
        }
    }
    public int IndexInGroup
    {
        get
        {
            return _indexInGroup;
        }
    }

    public void Apply()
    {
        float oldMaxHealth = PlayerStats.Instance.MaxHealth;
        PlayerStats.Instance.stats[_appliesTo] = PlayerStats.Instance.baseMaxHealth + (float)_amount;
        float newMaxHealth = PlayerStats.Instance.MaxHealth;
        PlayerStats.Instance.Health += newMaxHealth - oldMaxHealth;
        _enabled = true;
    }

    public void Unapply()
    {
        PlayerStats.Instance.stats[_appliesTo] = PlayerStats.Instance.baseMaxHealth;
        PlayerStats.Instance.Health = Mathf.Clamp(PlayerStats.Instance.Health, 0f, PlayerStats.Instance.baseMaxHealth);
        _enabled = false;
    }
}

// Abilities
public class Gell : MonoBehaviour, IUpgrade
{
    string _name = "Gell";
    float _price = 2300f;
    bool _enabled = false;
    bool _unlocked = false;
    string _flavour = "Protect yourself from those nasty jellyfish";
    string _upgradeGroup = "";

    string _appliesTo = "";
    float _amount = 0;
    int _indexInGroup = -1;

    public string Name
    {
        get
        {
            return _name;
        }
    }
    public float Price
    {
        get
        {
            return _price;
        }
    }
    public bool Enabled
    {
        get
        {
            return _enabled;
        }

        set
        {
            _enabled = value;
        }
    }
    public bool Unlocked
    {
        get
        {
            return _unlocked;
        }

        set
        {
            _unlocked = value;
        }
    }
    public string Flavour
    {
        get
        {
            return _flavour;
        }
    }
    public string UpgradeGroup
    {
        get
        {
            return _upgradeGroup;
        }
    }

    public string AppliesTo
    {
        get
        {
            return _appliesTo;
        }
    }

    public object Amount
    {
        get
        {
            return _amount;
        }
    }

    public int IndexInGroup
    {
        get
        {
            return _indexInGroup;
        }
    }

    public void Apply()
    {
        _enabled = true;
    }

    public void Unapply()
    {
        _enabled = false;
    }
}

public class Lamp : MonoBehaviour, IUpgrade
{
    string _name = "Lamp";
    float _price = 1400f;
    bool _enabled = false;
    bool _unlocked = false;
    string _flavour = "See more in the darkness";
    string _upgradeGroup = "";

    string _appliesTo = "";
    float _amount = 0;
    int _indexInGroup = -1;

    public string Name
    {
        get
        {
            return _name;
        }
    }
    public float Price
    {
        get
        {
            return _price;
        }
    }
    public bool Enabled
    {
        get
        {
            return _enabled;
        }

        set
        {
            _enabled = value;
        }
    }
    public bool Unlocked
    {
        get
        {
            return _unlocked;
        }

        set
        {
            _unlocked = value;
        }
    }
    public string Flavour
    {
        get
        {
            return _flavour;
        }
    }
    public string UpgradeGroup
    {
        get
        {
            return _upgradeGroup;
        }
    }

    public string AppliesTo
    {
        get
        {
            return _appliesTo;
        }
    }

    public object Amount
    {
        get
        {
            return _amount;
        }
    }

    public int IndexInGroup
    {
        get
        {
            return _indexInGroup;
        }
    }

    public void Apply()
    {
        _enabled = true;
    }

    public void Unapply()
    {
        _enabled = false;
    }
}