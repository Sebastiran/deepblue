﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeManager : MonoBehaviour
{
    public List<IUpgrade> upgrades = new List<IUpgrade>();

    private void Awake()
    {
        upgrades.Add(new OxygenTier1());
        upgrades.Add(new OxygenTier2());
        upgrades.Add(new CargoTier1());
        upgrades.Add(new DepthTier1());
        upgrades.Add(new ArmorTier1());
        upgrades.Add(new HealthTier1());

        upgrades.Add(new Gell());
        upgrades.Add(new Lamp());

        upgrades = PlayerPrefsManager.Instance.LoadUpgrades(upgrades);
        ApplyUpgrades();
    }

    public void Apply(IUpgrade upgrade)
    {
        if (upgrade.UpgradeGroup != "")
        {
            foreach (IUpgrade u in upgrades)
            {
                if (u.UpgradeGroup == upgrade.UpgradeGroup)
                {
                    u.Unapply();
                }
            }
        }
        upgrade.Apply();
        PlayerStats.discreteStatChanged();
    }

    public void Unapply(IUpgrade upgrade)
    {
        upgrade.Unapply();
        PlayerStats.discreteStatChanged();
    }

    public void UnapplyUpgradeGroup(string group)
    {
        foreach (IUpgrade upgrade in upgrades)
        {
            if (upgrade.UpgradeGroup == group)
            {
                upgrade.Unapply();
            }
        }
        PlayerStats.discreteStatChanged();
    }

    public void ApplyUpgrades()
    {
        foreach (IUpgrade upgrade in upgrades)
        {
            if (upgrade.Enabled)
            {
                Apply(upgrade);
            }
        }
        PlayerStats.discreteStatChanged();
    }

    public IUpgrade GetUpgradeByName(string name)
    {
        foreach (IUpgrade u in upgrades)
        {
            if (u.Name == name)
            {
                return u;
            }
        }
        return null;
    }

    public bool IsUpgradeUnlockable(IUpgrade upgrade)
    {
        foreach (IUpgrade u in upgrades)
        {
            if (u.UpgradeGroup == upgrade.UpgradeGroup)
            {
                if (u.IndexInGroup < upgrade.IndexInGroup)
                {
                    if (!u.Unlocked)
                    {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private void OnApplicationPause(bool pause)
    {
        PlayerPrefsManager.Instance.SaveUpgrades(upgrades);
    }

    private void OnApplicationQuit()
    {
        PlayerPrefsManager.Instance.SaveUpgrades(upgrades);
    }
}
