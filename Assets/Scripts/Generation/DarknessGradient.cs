﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarknessGradient : MonoBehaviour
{
    public Transform player;
    public float depthToChangeColor = 200f;
    public float depthToMaxDarkness = 270f;
    private int currentDepth = 0;

    MeshRenderer meshRenderer;

    void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        meshRenderer.sortingOrder = 10;
    }

    void Update()
    {
        float darkness = (-player.position.y - depthToChangeColor) / (depthToMaxDarkness - depthToChangeColor);
        darkness = Mathf.Clamp(darkness, 0f, 1f);

        if (GameManager.Instance.GetComponent<UpgradeManager>().GetUpgradeByName("Lamp").Enabled)
        {
            Color newColor = new Color(0f, 0f, 0f, darkness);
            meshRenderer.material.SetColor("_Color", newColor);
        }
        else
        {
            Color newColor = new Color(darkness, darkness, darkness, 1f);
            meshRenderer.material.SetColor("_SpecColor", newColor);
        }
    }
}
