﻿using UnityEngine;
using System.Collections.Generic;

public class Segment : MonoBehaviour
{
    public float height = 10f;
    public float minDepth = 0f;
    public float maxDepth = 0f;
    public float mass = 60f;
    public float massDeviation = 0f;
    public float rarity = 0f;
    public float fishRarity = 0f;

    public GameObject[] resources;

    private float currentMass = 0f;
    private float maxMass;

    [SerializeField]
    private bool active = true;

    private void Awake()
    {
        maxMass = Random.Range(-massDeviation, massDeviation) + mass;
    }

    /*private void FixedUpdate()
    {
        if (active)
        {
            if (Camera.main.transform.parent.position.y < transform.position.y - 20 || Camera.main.transform.parent.position.y > transform.position.y + 20)
            {
                active = false;
                SetActive(active);
            }
        }
        else
        {
            if (Camera.main.transform.parent.position.y > transform.position.y - 20 && Camera.main.transform.parent.position.y < transform.position.y + 20)
            {
                active = true;
                SetActive(active);
            }
        }
    }*/

    private void SetActive(bool active)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(active);
        }
    }

    public void SpawnFish()
    {
        while (currentMass < maxMass)
        {
            List<GameObject> possibleResourcesRarity = new List<GameObject>();
            float rarityResource = Random.Range(1f, 100f);
            rarityResource += fishRarity;
            for (int i = 0; i < resources.Length; i++)
            {
                Resource rr = resources[i].GetComponent<Resource>();
                if (rr.rarity <= rarityResource)
                {
                    possibleResourcesRarity.Add(resources[i]);
                }
            }
            GameObject resource;
            if (possibleResourcesRarity.Count > 0)
            {
                resource = possibleResourcesRarity[Random.Range(0, possibleResourcesRarity.Count)];
            }
            else
            {
                resource = resources[Random.Range(0, resources.Length)];
            }
            Resource r = resource.GetComponent<Resource>();
            GameObject newResource = Instantiate(resource, transform);
            PlayerPrefsManager.Instance.SetFishFound(r._name);

            newResource.transform.localPosition = new Vector3(Random.Range(-8f, 8f), Random.Range(-height, 0f), 0f);
            currentMass += r.mass;
        }
    }
}
