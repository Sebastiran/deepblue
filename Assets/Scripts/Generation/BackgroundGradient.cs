﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class BackgroundGradient : MonoBehaviour
{
    public Transform player;
    public float depthToChangeColor = 150f;
    private int currentDepth = 0;

    public Color startColor;
    public Color targetColor;
    public Color currentColor;

    public Color[] possibleColors;
    private List<Color> colorList = new List<Color>();

    MeshFilter meshFilter;
    MeshRenderer meshRenderer;

    void Awake()
    {
        meshFilter = GetComponent<MeshFilter>();
        meshRenderer = GetComponent<MeshRenderer>();
        meshRenderer.sortingOrder = -2;
        startColor = possibleColors[0];
        targetColor = possibleColors[Random.Range(0, possibleColors.Length)];
        colorList.Add(startColor);
        colorList.Add(targetColor);
    }

    void Update()
    {
        if ((currentDepth * depthToChangeColor) + player.position.y < -depthToChangeColor)
        {
            currentDepth++;
            startColor = colorList[currentDepth];
            if (colorList.Count > currentDepth + 1)
            {
                targetColor = colorList[currentDepth + 1];
            }
            else
            {
                targetColor = possibleColors[Random.Range(0, possibleColors.Length)];
                colorList.Add(targetColor);
            }
        }
        else if ((currentDepth * depthToChangeColor) + player.position.y > 0f)
        {
            if (currentDepth > 0)
            {
                currentDepth--;
                startColor = colorList[currentDepth];
                targetColor = colorList[currentDepth + 1];
            }
        }

        float t = ((-currentDepth * depthToChangeColor) - player.position.y) / depthToChangeColor;
        currentColor = Color.Lerp(startColor, targetColor, t);

        meshRenderer.sharedMaterial.color = currentColor;
        //meshFilter.sharedMesh.colors = new Color[] { Color.white, Color.white, Color.white, Color.white };
    }
}
