﻿using System.Collections.Generic;
using UnityEngine;

public class SeaFiller : MonoBehaviour
{
    public GameObject[] resources;
    public GameObject[] segments;
    public Transform player;
    public Transform seaParent;
    
    private float startAtDepth = 15f;
    private float generatedDepth = 0f;

    private List<GameObject> spawnedSegments = new List<GameObject>();

    private void Awake()
    {
        generatedDepth = startAtDepth;
        NewSegment();
        SubmarineMovement.aboveWater += ResetSegments;
    }

    private void NewSegment()
    {
        List<GameObject> possibleSegments = new List<GameObject>();
        float rarity = Random.Range(1f, 100f);
        for (int i = 0; i < segments.Length; i++)
        {
            Segment s = segments[i].GetComponent<Segment>();
            if (s.minDepth <= generatedDepth && (s.maxDepth >= generatedDepth || s.maxDepth == 0f) && s.rarity <= rarity)
            {
                possibleSegments.Add(segments[i]);
            }
        }
        if (possibleSegments.Count > 0)
        {
            GameObject segment = possibleSegments[Random.Range(0, possibleSegments.Count)];
            Segment s = segment.GetComponent<Segment>();
            GameObject newSegment = Instantiate(segment, seaParent);
            spawnedSegments.Add(newSegment);
            s = newSegment.GetComponent<Segment>();
            newSegment.transform.position = new Vector3(0f, -generatedDepth, 0f);
            generatedDepth += s.height;

            List<GameObject> possibleResources = new List<GameObject>();
            for (int i = 0; i < resources.Length; i++)
            {
                Resource r = resources[i].GetComponent<Resource>();
                if (r.minDepth <= generatedDepth && (r.maxDepth >= generatedDepth || r.maxDepth == 0f))
                {
                    possibleResources.Add(resources[i]);
                }
            }
            if (possibleResources.Count > 0)
            {
                s.resources = possibleResources.ToArray();
                s.SpawnFish();
            }
        }
    }

    private void ResetSegments()
    {
        if (spawnedSegments.Count > 0)
        {
            GameObject g = spawnedSegments[spawnedSegments.Count - 1];
            spawnedSegments.RemoveAt(spawnedSegments.Count - 1);
            Destroy(g);
        }
        if (spawnedSegments.Count > 0)
            ResetSegments();
        else
        {
            spawnedSegments.Clear();
            generatedDepth = startAtDepth;
        }
    }

    private void Update()
    {
        if (generatedDepth <= PlayerStats.Instance.Depth + 30f)
            NewSegment();
    }
}
