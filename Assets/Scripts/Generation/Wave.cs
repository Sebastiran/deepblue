﻿using System.Collections.Generic;
using UnityEngine;

public class Spring
{
    public float position;

    public float velocity;
    private float acceleration;
    private float mass;
    private float springConstant;
    private float damp;

    public Spring(float mass, float damp, float springConstant)
    {
        this.mass = mass;
        this.damp = damp;
        this.springConstant = springConstant;
    }

    public void Update()
    {
        acceleration = (-damp * velocity -springConstant * position) / mass;
        velocity += acceleration * Time.deltaTime;
        position += velocity * Time.deltaTime;
    }
}

[ExecuteInEditMode]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class Wave : MonoBehaviour
{
    private List<Spring> springs = new List<Spring>();
    public int springAmount = 40;
    public float mass = 1f;
    public float damp = 0.24f;
    public float springConstant = 0.1f;

    public float spread = 0.5f;
    public int pulls = 1;
    
    public MeshFilter meshFilter;
    public float waveStart = -10;
        
    public float springWidth = 1f;
    public float waveDepth = 2f;

    public int sortingOrder = 1;

    float zPosOfWave = 0f;
    float textureSize = 1f;
    
    private void Awake()
    {
        Init();
    }

    public void Init()
    {
        for (int i = 0; i < springAmount; i++)
        {
            springs.Add(new Spring(mass, damp, springConstant));
        }

        if (!meshFilter.sharedMesh)
            meshFilter.sharedMesh = new Mesh();
        meshFilter.mesh.name = "Wave";

        GetComponent<MeshRenderer>().sortingOrder = sortingOrder;

        leftDeltas = new float[springAmount];
        rightDeltas = new float[springAmount];
        terrainVertices = new Vector3[springAmount * 2];
        terrainTexCoords = new Vector2[springAmount * 2];
        triangles = new int[(springAmount - 1) * 6];
    }

    public void Splash(float position, float speed)
    {
        int index = (int)((position - waveStart) / springWidth);
        if (index >= 0 && index < springAmount)
            springs[index].velocity = speed;
    }

    public void Splash(int index, float speed)
    {
        if (index >= 0 && index < springAmount)
            springs[index].velocity = speed;
    }

    private void Update()
    {
        if (springs.Count < springAmount)
            Init();

        if (Camera.main.transform.parent.position.y > transform.position.y - 20)
        {
            for (int i = 0; i < springAmount; i++)
            {
                springs[i].Update();
            }
            Propagate();
            DrawMesh();
        }
    }

    float[] leftDeltas;
    float[] rightDeltas;

    private void Propagate()
    {
        for (int p = 0; p < pulls; p++)
        {
            for (int i = 0; i < springAmount; i++)
            {
                if (i > 0)
                {
                    leftDeltas[i] = (springs[i].position - springs[i - 1].position) * spread;
                    springs[i - 1].velocity += leftDeltas[i];
                }
                if (i < springAmount - 1)
                {
                    rightDeltas[i] = (springs[i].position - springs[i + 1].position) * spread;
                    springs[i + 1].velocity += rightDeltas[i];
                }
            }
        }

        for (int i = 0; i < springAmount; i++)
        {
            if (i > 0)
            {
                springs[i - 1].position += leftDeltas[i];
            }
            if (i < springAmount - 1)
            {
                springs[i + 1].position += rightDeltas[i];
            }
        }
    }

    Vector3[] terrainVertices;
    Vector2[] terrainTexCoords;
    int[] triangles;
    int triangleIndex = -2;

    private void DrawMesh()
    {
        triangleIndex = -2; // Start at -2 so we can skip the first 2 verts. 2 verts in a verticale line cant make a triangle
        
        for (int i = 0; i < springAmount; i++)
        {
            Vector2 point = new Vector2(i * springWidth, springs[i].position);
            terrainVertices[i * 2] = new Vector3(point.x + waveStart, point.y, zPosOfWave);
            terrainVertices[(i * 2) + 1] = new Vector3(point.x + waveStart, point.y - waveDepth, zPosOfWave);

            terrainTexCoords[i * 2] = new Vector2(point.x / textureSize, 1);
            terrainTexCoords[(i * 2) + 1] = new Vector2(point.x / textureSize, 0);

            if (triangleIndex >= 0)
            {
                triangles[(i - 1) * 6] = triangleIndex + 2;
                triangles[((i - 1) * 6) + 1] = triangleIndex + 1;
                triangles[((i - 1) * 6) + 2] = triangleIndex + 0;
                triangles[((i - 1) * 6) + 3] = triangleIndex + 3;
                triangles[((i - 1) * 6) + 4] = triangleIndex + 1;
                triangles[((i - 1) * 6) + 5] = triangleIndex + 2;
            }
            triangleIndex += 2;
        }

        // Setup the mesh
        var mesh = meshFilter.mesh;
        mesh.Clear();
        mesh.vertices = terrainVertices;
        mesh.uv = terrainTexCoords;
        mesh.triangles = triangles;
        mesh.RecalculateBounds();
    }
}