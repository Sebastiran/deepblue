﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class SubmarineMovement : MonoBehaviour
{
    public Wave wave;
    private Rigidbody2D _rigidBody;
    private PlayerStats stats;

    public delegate void Above();
    public static Above aboveWater;

    public enum Water
    {
        inWater = 0,
        onWater = 1,
        aboveWater = 2
    }

    public static int water = (int)Water.aboveWater;

    private void Awake()
    {
        // INPUTTYPE: Hold and drag to follow input
        TouchExtension.Instance.press += OnPress;

        stats = PlayerStats.Instance;
        _rigidBody = GetComponent<Rigidbody2D>();
        aboveWater += AboveWater;
        switch (water)
        {
            case 0:
                {
                    InWater();
                    break;
                }
            case 1:
                {
                    OnWater();
                    break;
                }
            case 2:
                {
                    aboveWater();
                    break;
                }
            default:
                {
                    InWater();
                    break;
                }
        }
    }

    private void InWater()
    {
        water = (int)Water.inWater;
        _rigidBody.gravityScale = 0;
        _rigidBody.drag = 3f;
    }

    private void OnWater()
    {
        wave.Splash(transform.position.x, _rigidBody.velocity.y * 4f);
        water = (int)Water.onWater;
        _rigidBody.gravityScale = 1f;
        _rigidBody.drag = 5f;
    }

    private void AboveWater()
    {
        wave.Splash(transform.position.x, _rigidBody.velocity.y * 4f);
        water = (int)Water.aboveWater;
        _rigidBody.gravityScale = 2f;
        _rigidBody.drag = 0f;
    }

    private void OnPress(Touch touch)
    {
        if (GameManager.Instance.allowControl)
        {
            // INPUTTYPE: Tap to lerp to position
            Vector2 targetPosition = GameManager.Instance.GetWorldPositionOnPlane(touch.position, 0f);
            Vector2 direction = (targetPosition - (Vector2)transform.position).normalized;

            float vertical = direction.y * stats.Speed * Time.deltaTime;
            float horizontal = direction.x * stats.Speed * Time.deltaTime;
            
            vertical = water != (int)Water.aboveWater ? vertical : 0;
            vertical = water != (int)Water.onWater ? vertical : vertical > 0 ? 0 : vertical;
            Vector2 force = new Vector2(horizontal, vertical);
            _rigidBody.AddForce(force);

            if (vertical < 0 && water == (int)Water.onWater)
            {
                InWater();
            }
        }
    }

    private void Update()
    {
#if UNITY_EDITOR
        if (GameManager.Instance.allowControl)
        {
            float horizontal = 0f;
            float vertical = 0f;
            horizontal = Input.GetAxis("Horizontal") * stats.Speed * Time.deltaTime;
            vertical = Input.GetAxis("Vertical") * stats.Speed * Time.deltaTime;
            
            vertical = water != (int)Water.aboveWater ? vertical : 0;
            vertical = water != (int)Water.onWater ? vertical : vertical > 0 ? 0 : vertical;
            Vector2 force = new Vector2(horizontal, vertical);
            _rigidBody.AddForce(force);
            if (vertical < 0 && water == (int)Water.onWater)
            {
                InWater();
            }
        }
#endif

        transform.rotation = Quaternion.AngleAxis(Mathf.Atan2(_rigidBody.velocity.y, _rigidBody.velocity.x) * Mathf.Rad2Deg, Vector3.forward);

        if (water == (int)Water.onWater)
        {
            wave.Splash(transform.position.x, _rigidBody.velocity.y * 4f);
        }
    }

    private void FixedUpdate()
    {
        if (transform.position.y > 0 && water == (int)Water.inWater)
        {
            aboveWater();
        }

        if (transform.position.y < 0 && water == (int)Water.aboveWater)
        {
            OnWater();
        }

        if (water == (int)Water.onWater)
        {
            BouyantPhysics();
        }

        if (water == (int)Water.aboveWater)
        {
            HorizontalDrag();
        }
    }

    private void BouyantPhysics()
    {
        // To float an object with 50% submerging in water we need the fore downward to be equal to the force upwards at y = 0 (50% submergment)
        // The force downwards is simple F = m * g. With m = V * p.
        // Our object has a mass of 1 kg, gravity is 9.91, thus F = 1 * 9.81 = 9.81
        // V = 1 thus p = m / V = 1 / 1 = 1 kg/m3
        // The upwards force needs to be equal to the downward force. F(u) = F(d) = 9.81
        // F = V * p * g = 9.81. We know g = 9.81 thus V * p = 1
        // V(s) = the amount of water displaced by the object. Which should be exactly 50% of the volume of the object.
        // V(b) = 1. Thus V(s) = 0.5
        // p = 1 / V = 1 / 0.5 = 2 kg/m3
        // Meaning if we want an object with a mass of 1 kg and a volume of 1 m3 to float with 50% submerging we would need a liquid with a density of 2 kg/m3
        
        float liquidDensity = 2f;       // kg/m3

        float depth = transform.position.y;
        depth = Mathf.Clamp(depth, -0.5f, 0.5f);
        float waterDisplacement = Mathf.Abs(depth - 0.5f);

        float bouyantForce = waterDisplacement * liquidDensity * 9.81f;
        
        _rigidBody.AddForce(new Vector2(0f, bouyantForce));
    }

    private void HorizontalDrag()
    {
        Vector2 velocity = _rigidBody.velocity;
        velocity.x *= 0.9f;
        _rigidBody.velocity = velocity;
    }
}