﻿using UnityEngine;

public class CameraFollowTarget : MonoBehaviour
{
    public Transform target;

    public float lookAheadFactor = 1f;
    public float zoomFactor = 1f;
    public float zoomSmooth = 2f;

    public float maxX = 5f;
    public float minX = -5f;

    public bool active = true;

    private Rigidbody2D rb;
    private Camera cam;
    private float defaultZPos;

    private void Awake()
    {
        rb = target.GetComponent<Rigidbody2D>();
        defaultZPos = transform.position.z;
        cam = Camera.main;
    }

    private void Update()
    {
        if (active)
        {
            Vector3 endPos = Vector3.zero;
            Vector3 targetPosition = target.position;
            targetPosition.z = transform.position.z;
            
            endPos = Vector3.Lerp(targetPosition, targetPosition + ((Vector3)rb.velocity * lookAheadFactor), 2f * Time.deltaTime); //targetPosition + ((Vector3)rb.velocity * lookAheadFactor);
            endPos.z = Mathf.Lerp(endPos.z, defaultZPos - (rb.velocity.magnitude * zoomFactor), zoomSmooth * Time.deltaTime);

            transform.position = endPos;
         
        }

        if (transform.position.x < minX)
        {
            transform.position = new Vector3(minX, transform.position.y, transform.position.z);
        }
        else if (transform.position.x > maxX)
        {
            transform.position = new Vector3(maxX, transform.position.y, transform.position.z);
        }
    }
}