﻿using UnityEngine;
using System.Collections;

public class Resource : MonoBehaviour
{
    public enum SwimType
    {
        None,
        Swim
    }

    public enum ReactType
    {
        None,
        Flee,
        FleeDown,
        Fight
    }

    public SwimType swimType = SwimType.None;
    public ISwimBehaviour swimBehaviour;
    public ReactType reactType = ReactType.None;
    public IReactBehaviour reactBehaviour;
    public bool initializeBehaviours = true;

    public string _name = "fish";

    public float value = 100f;
    public float mass = 10f;
    public float minDepth = 2f;
    public float maxDepth = 20f;
    public float rarity = 0f;
    public float health = 10f;
    public float damage = 0f;
    public float reactionRange = 3f;

    public bool bigFish = false;
    public bool attached = false;
    public LayerMask reactMask;

    public Transform lastReactTo;
    public Harpoon harpoon;
    public bool reacting = false;
    private bool dead = false;
    public bool Dead
    {
        get
        {
            return dead;
        }
        set
        {
            dead = value;
            if (dead)
            {
                if (swimType != SwimType.None)
                    swimBehaviour.Active = false;
                if (reactType != ReactType.None)
                    reactBehaviour.Active = false;
                // Dangerous code, should take the physical collider of the resource and turn it off. Otherwise if you real in a fish from above the skycollider the fish will get stuck.
                GetComponentsInChildren<Collider2D>()[1].enabled = false;
            }
        }
    }

    private void Awake()
    {
        Init();
    }

    private void Update()
    {
        RaycastHit2D hit = Physics2D.CircleCast(transform.position - (Vector3.forward * 5f), reactionRange, Vector3.forward, 10f, reactMask);
        if (hit)
            React(hit.transform);

        if (attached &&bigFish && !reacting)
        {
            if(!lastReactTo)
                lastReactTo = GameObject.FindGameObjectWithTag("Player").transform;
            React(lastReactTo);
        }
    }

    protected virtual void Init()
    {
        harpoon = GameObject.FindGameObjectWithTag("Harpoon").GetComponent<Harpoon>();
        HandleSwimType();
        HandleReactType();
    }

    protected void HandleSwimType()
    {
        Component c = gameObject.GetComponent<ISwimBehaviour>() as Component;
        if (c != null)
        {
            if (!initializeBehaviours)
            {
                swimBehaviour = (ISwimBehaviour)c;
                return;
            }
            Destroy(c);
        }

        switch (swimType)
        {
            case SwimType.Swim:
                swimBehaviour = gameObject.AddComponent<Swim>();
                break;
            default:
                break;
        }
    }

    protected void HandleReactType()
    {
        Component c = gameObject.GetComponent<IReactBehaviour>() as Component;
        if (c != null)
        {
            Destroy(c);
        }

        switch (reactType)
        {
            case ReactType.Flee:
                reactBehaviour = gameObject.AddComponent<Flee>();
                break;
            case ReactType.FleeDown:
                reactBehaviour = gameObject.AddComponent<FleeDown>();
                break;
            case ReactType.Fight:
                reactBehaviour = gameObject.AddComponent<Fight>();
                break;
            default:
                break;
        }
    }

    public virtual void React(Transform reactTo)
    {
        if (reactBehaviour != null)
        {
            reactBehaviour.React(reactTo);
        }
    }

    private void OnMouseEnter()
    {
        harpoon.Shoot();
    }

    private void OnMouseDown()
    {
        harpoon.Shoot();
    }

    public virtual void Hit()
    {
        attached = true;
        AudioManager.Instance.PlaySound("Attach");

        if (bigFish)
        {
            StartCoroutine(Bleed());
        }
        else
        {
            Dead = true;
        }
    }

    protected IEnumerator Bleed()
    {
        while (health > 0)
        {
            health -= PlayerStats.Instance.Damage;
            if (health <= 0)
            {
                Dead = true;
                GetComponent<SpriteRenderer>().flipY = true;
            }
            yield return new WaitForSeconds(1f);
        }
    }

    public virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && !Dead)
        {
            if (damage > 0f)
                collision.gameObject.GetComponent<Submarine>().ApplyDamage(damage);
        }
    }
}
