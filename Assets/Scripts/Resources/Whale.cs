﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Whale : Resource
{
    public override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
        if (collision.gameObject.tag == "Harpoon")
        {
            reactType = ReactType.FleeDown;
            HandleReactType();
        }
    }
}