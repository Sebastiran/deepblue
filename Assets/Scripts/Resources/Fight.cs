﻿using System.Collections;
using UnityEngine;

public class Fight : BaseReact
{
    public override void React(Transform reactTo)
    {
        base.React(reactTo);
        if (resource.reacting || !Active)
            return;

        Vector3 deltaPosition = reactTo.position - transform.position;
        StartCoroutine(Stroke(deltaPosition));
    }

    private IEnumerator Stroke(Vector3 deltaPosition)
    {
        resource.reacting = true;
        int strokeAmount = 3;
        int strokes = 0;
        while (strokes < strokeAmount)
        {
            GetComponent<Rigidbody2D>().AddForce(deltaPosition.normalized * strokeForce);
            strokes++;
            yield return new WaitForSeconds(timeBetweenStrokes);
        }

        GetComponent<Resource>().swimBehaviour.Active = true;
        resource.reacting = false;
    }
}