﻿using UnityEngine;

public class Jellyfish : Resource
{
    public override void OnTriggerEnter2D(Collider2D collision)
    {
        // Don't go to base, we overwrite the method to check if the gell is enabled
        if (collision.gameObject.tag == "Player" && !Dead && !GameManager.Instance.GetComponent<UpgradeManager>().GetUpgradeByName("Gell").Enabled)
        {
            if (damage > 0f)
                collision.gameObject.GetComponent<Submarine>().ApplyDamage(damage);
        }
        if (collision.gameObject.tag == "Player" && !attached)
        {
            gameObject.SetActive(false);
        }
    }
}
