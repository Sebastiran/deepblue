﻿using System.Collections;
using UnityEngine;

public class Swim : MonoBehaviour, ISwimBehaviour
{
    [SerializeField]
    private float strokeForce = 5f;
    [SerializeField]
    private int strokeAmount = 2;
    [SerializeField]
    private int deltaStrokeAmount = 1;
    [SerializeField]
    private float timeBetweenStrokes = 0.2f;

    [SerializeField]
    private float strokeTime = 2f;
    [SerializeField]
    private float deltaStrokeTime = 1.5f;
    [SerializeField]
    private float turnTime = 6f;
    [SerializeField]
    private float deltaTurnTime = 3f;

    [SerializeField]
    private float verticalDeviation = 1f;
    private float direction = 1;

    private bool _active = true;
    private bool stroking = false;
    [SerializeField]
    private bool strokeAfterTurn = false;

    private Rigidbody2D rigidBody;

    public bool Active
    {
        get
        {
            return _active;
        }
        set
        {
            _active = value;
            if (_active)
            {
                StartCoroutine(Swimming());
                StartCoroutine(Turn());
            }
            else
            {
                StopAllCoroutines();
            }
        }
    }

    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        direction = Random.Range(0, 2) * 2 - 1;
        StartCoroutine(Swimming());
        StartCoroutine(Turn());
    }

    private IEnumerator Stroke()
    {
        stroking = true;
        int strokeAmount = Random.Range(this.strokeAmount - deltaStrokeAmount, this.strokeAmount + deltaStrokeAmount + 1);
        int strokes = 0;
        float deviation = Random.Range(-verticalDeviation, verticalDeviation);
        Vector2 force = new Vector2(strokeForce * direction, deviation);
        while (strokes < strokeAmount)
        {
            rigidBody.AddForce(force, ForceMode2D.Force);
            strokes++;
            yield return new WaitForSeconds(timeBetweenStrokes);
        }
        stroking = false;
    }

    private IEnumerator Swimming()
    {
        //Wait a random time before we start to make sure not all fish swim and stroke in sync if the delta is 0
        yield return new WaitForSeconds(Random.Range(0f, 2f));
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(strokeTime - deltaStrokeTime, strokeTime + deltaStrokeTime));
            if (!stroking)
                StartCoroutine(Stroke());
        }
    }

    private IEnumerator Turn()
    {
        //Wait a random time before we start to make sure not all fish turn in sync if the delta is 0
        yield return new WaitForSeconds(Random.Range(0f, 2f));
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(turnTime - deltaTurnTime, turnTime + deltaTurnTime));
            direction *= -1;
            if (strokeAfterTurn)
                StartCoroutine(Stroke());
        }
    }
}