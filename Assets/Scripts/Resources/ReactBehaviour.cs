﻿using UnityEngine;
using System.Collections;

public interface IReactBehaviour
{
    bool Active { get; set; }
    void React(Transform reactTo);
}

public class BaseReact : MonoBehaviour, IReactBehaviour
{
    public float timeBetweenStrokes = 0.2f;
    public float strokeForce = 25f;
    
    protected Resource resource;

    private bool _active = true;
    public bool Active
    {
        get
        {
            return _active;
        }
        set
        {
            _active = value;
        }
    }

    private void Awake()
    {
        Init();
    }

    protected virtual void Init()
    {
        resource = GetComponent<Resource>();
    }

    public virtual void React(Transform reactTo)
    {
        if (resource.reacting || !Active)
            return;

        resource.swimBehaviour.Active = false;
        resource.lastReactTo = reactTo;
    }
}

public class Flee : BaseReact
{
    protected override void Init()
    {
        base.Init();
        timeBetweenStrokes = 0.2f;
        strokeForce = 25f;
    }

    public override void React(Transform reactTo)
    {
        base.React(reactTo);
        if (resource.reacting || !Active)
            return;
        
        Vector3 deltaPosition = reactTo.position - transform.position;
        StartCoroutine(Stroke(deltaPosition));
    }

    private IEnumerator Stroke(Vector3 deltaPosition)
    {
        Vector3 direction = -deltaPosition.normalized;
        if (Mathf.Abs(direction.y) > 0.7f)
        {
            direction = new Vector3(direction.x < 0 ? -1 : 1, direction.y < 0 ? -1 : 1, 0f).normalized;
        }
        resource.reacting = true;
        int strokeAmount = 3;
        int strokes = 0;
        while (strokes < strokeAmount)
        {
            GetComponent<Rigidbody2D>().AddForce(direction * strokeForce);
            strokes++;
            yield return new WaitForSeconds(timeBetweenStrokes);
        }

        GetComponent<Resource>().swimBehaviour.Active = true;
        resource.reacting = false;
    }
}

public class FleeDown : BaseReact
{
    protected override void Init()
    {
        base.Init();
        timeBetweenStrokes = 0.4f;
        strokeForce = 25f;
    }

    public override void React(Transform reactTo)
    {
        base.React(reactTo);
        if (resource.reacting || !Active)
            return;
        
        StartCoroutine(Stroke());
    }

    private IEnumerator Stroke()
    {
        resource.reacting = true;
        int strokeAmount = 3;
        int strokes = 0;
        while (strokes < strokeAmount)
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.down * strokeForce);
            strokes++;
            yield return new WaitForSeconds(timeBetweenStrokes);
        }

        GetComponent<Resource>().swimBehaviour.Active = true;
        resource.reacting = false;
    }
}