﻿using Beast;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    public Transform spawnPosition;
    public GameObject player;
    public GameObject harpoon;

    public bool allowControl = true;
    private bool pause = false;
    public bool Pause
    {
        get
        {
            return pause;
        }
        set
        {
            pause = value;
            allowControl = !value;
        }
    }
    public float respawnTime = 3f;

    public void Die()
    {
        allowControl = false;
        Invoke("Respawn", respawnTime);
    }

    private void Respawn()
    {
        allowControl = true;
        PlayerStats.Instance.Money = 0f;
        PlayerStats.Instance.CurrentCargo = 0f;
        PlayerStats.Instance.Health = PlayerStats.Instance.MaxHealth;
        PlayerStats.Instance.resources.Clear();
        PlayerStats.Instance.Oxygen = PlayerStats.Instance.MaxOxygen;

        harpoon.GetComponent<Harpoon>().Clear();

        player.transform.position = spawnPosition.position;
        player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        player.GetComponent<Submarine>().dead = false;
    }

    public Resource GetResourceByName(string name)
    {
        GameObject[] resourceObjects = GetComponent<SeaFiller>().resources;
        foreach (GameObject go in resourceObjects)
        {
            if (go.GetComponent<Resource>()._name == name)
            {
                return go.GetComponent<Resource>();
            }
        }
        return null;
    }

    public Vector3 GetWorldPositionOnPlane(Vector3 screenPosition, float z)
    {
        Ray ray = Camera.main.ScreenPointToRay(screenPosition);
        Plane xy = new Plane(Vector3.forward, new Vector3(0, 0, z));
        float distance;
        xy.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }
}
