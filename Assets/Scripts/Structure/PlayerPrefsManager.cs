﻿using UnityEngine;
using System.Collections.Generic;
using Beast;

public class PlayerPrefsManager : Singleton<PlayerPrefsManager>
{
    const string KEY_MONEY = "money";
    const string KEY_OXYGEN = "oxygen";
    const string KEY_CARGO = "cargo";
    const string KEY_ARMOR = "armor";
    const string KEY_DEPTH = "depth";
    const string KEY_HEALTH = "health";
    const string KEY_DAMAGE = "damage";
    const string KEY_MAX_HEALTH = "max_health";

    /** Meant for developer mode only! Total reset of everything saved. Be careful.*/
    public void DeleteAll()
    {
        PlayerPrefs.DeleteAll();
    }
    
    public float GetMoney()
    {
        return PlayerPrefs.GetFloat(KEY_MONEY, PlayerStats.Instance.Money);
    }
    public void SetMoney(float value)
    {
        PlayerPrefs.SetFloat(KEY_MONEY, value);
        PlayerPrefs.Save();
    }

    public float GetOxygen()
    {
        return PlayerPrefs.GetFloat(KEY_OXYGEN, PlayerStats.Instance.baseOxygen);
    }
    public void SetOxygen(float value)
    {
        PlayerPrefs.SetFloat(KEY_OXYGEN, value);
        PlayerPrefs.Save();
    }

    public float GetCargo()
    {
        return PlayerPrefs.GetFloat(KEY_CARGO, PlayerStats.Instance.baseMaxCargo);
    }
    public void SetCargo(float value)
    {
        PlayerPrefs.SetFloat(KEY_CARGO, value);
        PlayerPrefs.Save();
    }

    public float GetArmor()
    {
        return PlayerPrefs.GetFloat(KEY_ARMOR, PlayerStats.Instance.baseArmor);
    }
    public void SetArmor(float value)
    {
        PlayerPrefs.SetFloat(KEY_ARMOR, value);
        PlayerPrefs.Save();
    }

    public float GetDepth()
    {
        return PlayerPrefs.GetFloat(KEY_DEPTH, PlayerStats.Instance.baseDepth);
    }
    public void SetDepth(float value)
    {
        PlayerPrefs.SetFloat(KEY_DEPTH, value);
        PlayerPrefs.Save();
    }

    public float GetHealth()
    {
        return PlayerPrefs.GetFloat(KEY_HEALTH, PlayerStats.Instance.baseMaxHealth);
    }
    public void SetHealth(float value)
    {
        PlayerPrefs.SetFloat(KEY_HEALTH, value);
        PlayerPrefs.Save();
    }

    public float GetDamage()
    {
        return PlayerPrefs.GetFloat(KEY_DAMAGE, PlayerStats.Instance.baseDamage);
    }
    public void SetDamage(float value)
    {
        PlayerPrefs.SetFloat(KEY_DAMAGE, value);
        PlayerPrefs.Save();
    }

    public float GetMaxHealth()
    {
        return PlayerPrefs.GetFloat(KEY_MAX_HEALTH, PlayerStats.Instance.baseMaxHealth);
    }
    public void SetMaxHealth(float value)
    {
        PlayerPrefs.SetFloat(KEY_MAX_HEALTH, value);
        PlayerPrefs.Save();
    }

    public void SaveUpgrades(List<IUpgrade> upgrades)
    {
        foreach (IUpgrade u in upgrades)
        {
            PlayerPrefs.SetInt(u.Name + "_unlocked", u.Unlocked ? 1: 0);
            PlayerPrefs.SetInt(u.Name + "_enabled", u.Enabled ? 1 : 0);
        }
        PlayerPrefs.Save();
    }
    public List<IUpgrade> LoadUpgrades(List<IUpgrade> upgrades)
    {
        for (int i = 0; i < upgrades.Count; i++)
        {
            upgrades[i].Unlocked = PlayerPrefs.GetInt(upgrades[i].Name + "_unlocked", 0) == 1 ? true : false;
            upgrades[i].Enabled = PlayerPrefs.GetInt(upgrades[i].Name + "_enabled", 0) == 1 ? true : false;
        }
        return upgrades;
    }

    public void SaveFish(string fishName)
    {
        PlayerPrefs.SetInt(fishName + "_amount", PlayerPrefs.GetInt(fishName + "_amount", 0) + 1);
        PlayerPrefs.SetInt(fishName + "_found", 1);
        PlayerPrefs.SetInt(fishName + "_caught", 1);
    }
    public void SetFishFound(string fishName)
    {
        PlayerPrefs.SetInt(fishName + "_found", 1);
    }
    public bool FishFound(string fishName)
    {
        return PlayerPrefs.GetInt(fishName + "_found", 0) == 1 ? true : false;
    }
    public bool FishCaught(string fishName)
    {
        return PlayerPrefs.GetInt(fishName + "_caught", 0) == 1 ? true : false;
    }
    public int FishAmount(string fishName)
    {
        return PlayerPrefs.GetInt(fishName + "_amount", 0);
    }
}
