﻿using UnityEngine;

/** Makes a class a semi singleton. It is available to all*/
public abstract class Instancer<T> : MonoBehaviour where T : Instancer<T>
{
    private static T m_Instance = null;

    public static T Instance
    {
        get
        {
            // Instance requiered for the first time, we look for it
            if (m_Instance == null)
            {
                m_Instance = GameObject.FindObjectOfType(typeof(T)) as T;

                // Object not found
                if (m_Instance == null)
                {
                    Debug.LogError("No instance of " + typeof(T).ToString());
                }
            }
            return m_Instance;
        }
    }

    // If no other monobehaviour request the instance in an awake function
    // executing before this one, no need to search the object.
    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this as T;
        }
        else if (m_Instance != this)
        {
            Debug.LogError("Another instance of " + GetType() + " is already exist! Destroying self...");
            DestroyImmediate(this);
            return;
        }
    }

    /// Make sure the instance isn't referenced anymore when the user quit, just in case.
    private void OnApplicationQuit()
    {
        m_Instance = null;
    }
}