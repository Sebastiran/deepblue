﻿using UnityEngine;
using System.Collections;
using Beast;

public class TouchExtension : Singleton<TouchExtension>
{
    public int touchAmount = 2;
    
    public delegate void Tap(Touch touch);
    public Tap tap;
    public delegate void Press(Touch touch);
    public Press press;

    private void Update()
    {
        Touch[] touches = Input.touches;

        // loop through all currently active touches
        for (int i = 0; i < Input.touches.Length; i++)
        {
            // if this touch is above the touchLimit, do nothing
            if (i >= touches.Length)
                return;

            // if the touch just began
            if (touches[i].phase == TouchPhase.Began)
            {
                // notify all tap subscribers
                tap(touches[i]);
            }
            else 
            {
                press(touches[i]);
            }
        }
    }
}