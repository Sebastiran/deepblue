﻿using System.Collections;
using UnityEngine;
using Beast;

public class CameraShake : MonoBehaviour
{
    public Camera mainCam;

    public bool shaking = false;

    private float shakeAmount = 0;

    private void Awake()
    {
        if (mainCam == null)
        {
            mainCam = Camera.main;
        }
    }

    public void Shake(float amount, float length)
    {
        shakeAmount = amount;
        shaking = true;
        InvokeRepeating("DoShake", 0f, 0.01f);
        Invoke("StopShake", length);
    }

    private void DoShake()
    {
        if (shakeAmount > 0)
        {
            Vector3 camPos = mainCam.transform.position;

            float offsetX = Random.value * shakeAmount * 2 - shakeAmount;
            float offsetY = Random.value * shakeAmount * 2 - shakeAmount;
            camPos.x += offsetX;
            camPos.y += offsetY;

            mainCam.transform.position = camPos;
        }
    }

    private void StopShake()
    {
        CancelInvoke("DoShake");
        mainCam.transform.localPosition = Vector3.zero;
        shaking = false;
    }
}
