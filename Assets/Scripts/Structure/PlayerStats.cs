﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Beast;

public class PlayerStats : Singleton<PlayerStats>
{
    public float baseSpeed = 100f;
    public float baseOxygen = 100f;
    public float baseMaxCargo = 100f;
    public float baseArmor = 0f;
    public float baseDepth = 100f;
    public float baseDamage = 10f;
    public float baseMaxHealth = 100f;
    
    public float oxygenUse = 5f;

    private float money = 100f;
    private float health = 100f;
    private float depth = 0f;
    private float oxygen = 100f;
    private float currentCargo = 0f;

    public float Money
    {
        get
        {
            return money;
        }
        set
        {
            money = value;
            discreteStatChanged();
        }
    }
    public float Health
    {
        get
        {
            return health;
        }
        set
        {
            health = Mathf.Clamp(value, 0f, MaxHealth);
            discreteStatChanged();
        }
    }
    public float Depth
    {
        get
        {
            return depth;
        }
        set
        {
            depth = Mathf.Clamp(value, 0f, value);
        }
    }
    public float Oxygen
    {
        get
        {
            return oxygen;
        }
        set
        {
            oxygen = Mathf.Clamp(value, 0f, MaxOxygen);
        }
    }
    public float CurrentCargo
    {
        get
        {
            return currentCargo;
        }
        set
        {
            currentCargo = Mathf.Clamp(value, 0f, MaxCargo);
            discreteStatChanged();
        }
    }
    
    public float Speed
    {
        get
        {
            return (float)stats["speed"];
        }
    }
    public float MaxOxygen
    {
        get
        {
            return (float)stats["maxOxygen"];
        }
    }
    public float MaxCargo
    {
        get
        {
            return (float)stats["maxCargo"];
        }
    }
    public float Armor
    {
        get
        {
            return (float)stats["armor"];
        }
    }
    public float MaxDepth
    {
        get
        {
            return (float)stats["maxDepth"];
        }
    }
    public float Damage
    {
        get
        {
            return (float)stats["damage"];
        }
    }
    public float MaxHealth
    {
        get
        {
            return (float)stats["maxHealth"];
        }
    }

    public List<Resource> resources = new List<Resource>();

    public Dictionary<string, object> stats = new Dictionary<string, object>();

    private PlayerPrefsManager prefs;

    public delegate void DiscreteStatUpdate();
    public static DiscreteStatUpdate discreteStatChanged;

    private void Awake()
    {
        discreteStatChanged += InitDelegate;
        prefs = PlayerPrefsManager.Instance;

        money = prefs.GetMoney();
        health = prefs.GetHealth();
        
        stats.Add("speed", baseSpeed);

        stats.Add("maxOxygen", prefs.GetOxygen());
        stats.Add("maxCargo", prefs.GetCargo());
        stats.Add("armor", prefs.GetArmor());
        stats.Add("maxDepth", prefs.GetDepth());
        stats.Add("damage", prefs.GetDamage());
        stats.Add("maxHealth", prefs.GetMaxHealth());

        Oxygen = MaxOxygen;
    }

    private void InitDelegate() {}

    private void OnApplicationPause(bool pause)
    {
        if (!pause)
            return;

        prefs.SetMoney(money);
        prefs.SetHealth(health);

        prefs.SetOxygen(MaxOxygen);
        prefs.SetCargo(MaxCargo);
        prefs.SetArmor(Armor);
        prefs.SetDepth(MaxDepth);
        prefs.SetDamage(Damage);
        prefs.SetMaxHealth(MaxHealth);
    }

    private void OnApplicationQuit()
    {
        prefs.SetMoney(money);
        prefs.SetHealth(health);

        prefs.SetOxygen(MaxOxygen);
        prefs.SetCargo(MaxCargo);
        prefs.SetArmor(Armor);
        prefs.SetDepth(MaxDepth);
        prefs.SetDamage(Damage);
        prefs.SetMaxHealth(MaxHealth);
    }
}