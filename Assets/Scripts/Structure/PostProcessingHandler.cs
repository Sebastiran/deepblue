﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class PostProcessingHandler : MonoBehaviour
{
    public PostProcessingBehaviour ppb;
    private VignetteModel.Settings vignetteSettings;
    private MotionBlurModel.Settings motionBlurSettings;

    private void Awake()
    {
        vignetteSettings = ppb.profile.vignette.settings;
        motionBlurSettings = ppb.profile.motionBlur.settings;
    }

    private void Update()
    {
        vignetteSettings.smoothness = Mathf.Clamp((((PlayerStats.Instance.MaxOxygen - PlayerStats.Instance.Oxygen) / PlayerStats.Instance.MaxOxygen) - 0.5f) * 2f, 0f, 1f);
        ppb.profile.vignette.settings = vignetteSettings;

        motionBlurSettings.frameBlending = Mathf.Clamp((((PlayerStats.Instance.MaxOxygen - PlayerStats.Instance.Oxygen) / PlayerStats.Instance.MaxOxygen) - 0.75f) * 4f, 0f, 1f);
        ppb.profile.motionBlur.settings = motionBlurSettings;
    }
}
