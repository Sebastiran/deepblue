﻿using UnityEngine;

namespace Beast
{
    /// <summary>
    /// Be aware this will not prevent a non singleton constructor
    ///   such as `T myT = new T();`
    /// To prevent that, add `protected T () {}` to your singleton class.
    /// 
    /// As a note, this is made as MonoBehaviour because we need Coroutines.
    /// </summary>
    public abstract class Singleton<T> : MonoBehaviour where T : Singleton<T>
    {
        private static bool applicationIsQuitting = false;
        private static T m_Instance = null;

        public static T Instance
        {
            get
            {
                // Instance requiered for the first time, we look for it
                if (m_Instance == null)
                {
                    m_Instance = GameObject.FindObjectOfType(typeof(T)) as T;

                    // Object not found, we create a temporary one
                    if (m_Instance == null)
                    {
                        Debug.LogWarning("No instance of " + typeof(T).ToString() + ", a temporary one is created.");

                        if (!applicationIsQuitting)
                        {
                            m_Instance = new GameObject(typeof(T).ToString(), typeof(T)).GetComponent<T>();

                            DontDestroyOnLoad(m_Instance);

                            // Problem during the creation, this should not happen
                            if (m_Instance == null)
                            {
                                Debug.LogError("Problem during the creation of " + typeof(T).ToString());
                            }
                        }
                        else
                        {
                            Debug.Log("Application quitting, not making a new object");
                        }
                    }
                    if (!_isInitialized)
                    {
                        _isInitialized = true;
                    }
                }
                return m_Instance;
            }
        }

        private static bool _isInitialized;

        // If no other monobehaviour request the instance in an awake function
        // executing before this one, no need to search the object.
        private void Awake()
        {
            if (m_Instance == null)
            {
                m_Instance = this as T;
            }
            else if (m_Instance != this)
            {
                Debug.LogError("Another instance of " + GetType() + " is already exist! Destroying self...");
                DestroyImmediate(this);
                return;
            }
            if (!_isInitialized)
            {
                DontDestroyOnLoad(gameObject);
                _isInitialized = true;
            }
        }

        /// Make sure the instance isn't referenced anymore when the user quit, just in case.
        private void OnApplicationQuit()
        {
            applicationIsQuitting = true;
            m_Instance = null;
        }

        private void OnDisable()
        {
            applicationIsQuitting = true;
        }
    }
}