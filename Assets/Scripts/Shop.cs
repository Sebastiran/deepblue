﻿using UnityEngine;

public class Shop : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            if (!WindowManager.Instance.IsOpen((int)WindowManager.Window.Shop) && SubmarineMovement.water != (int)SubmarineMovement.Water.inWater)
            {
                GameManager.Instance.Pause = true;
                WindowManager.Instance.Open((int)WindowManager.Window.Shop);
            }
        }
    }
}
