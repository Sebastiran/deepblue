﻿using UnityEngine;
using System.Collections;

public class Submarine : MonoBehaviour
{
    PlayerStats stats;
    public bool dead = false;

    private void Awake()
    {
        stats = PlayerStats.Instance;
        StartCoroutine(CheckDepth());
    }

    private void Update()
    {
        stats.Depth = -transform.position.y;
        if (SubmarineMovement.water == (int)SubmarineMovement.Water.inWater)
        {
            stats.Oxygen -= stats.oxygenUse * Time.deltaTime;
            Mathf.Clamp(stats.Oxygen, 0f, stats.MaxOxygen);
        }
        if (stats.Oxygen <= 0f)
        {
            stats.Health -= 20f * Time.deltaTime;
        }

        if (stats.Health <= 0f)
        {
            Die();
        }
    }

    private IEnumerator CheckDepth()
    {
        while (true)
        {
            if (stats.MaxDepth < stats.Depth)
            {
                ApplyDamage(10f);
            }
            yield return new WaitForSeconds(3f);
        }
    }

    public void Collect(Resource resource)
    {
        if(stats.CurrentCargo + resource.mass <= stats.MaxCargo)
        {
            stats.CurrentCargo += resource.mass;
            stats.resources.Add(resource);
        }
        PlayerPrefsManager.Instance.SaveFish(resource._name);
    }

    private void FillOxygen()
    {
        StartCoroutine(FillOxygenCoroutine());
    }

    private IEnumerator FillOxygenCoroutine()
    {
        while (stats.Oxygen < stats.MaxOxygen && SubmarineMovement.water != (int)SubmarineMovement.Water.inWater)
        {
            stats.Oxygen += (stats.MaxOxygen / 2.5f) * Time.deltaTime;
            yield return null;
        }
    }

    public void ApplyDamage(float damage)
    {
        stats.Health -= damage * (1 - (stats.Armor / 100f));
        StartCoroutine(FlashSprite());
    }

    private IEnumerator FlashSprite()
    {
        float time = 0;
        float endTime = 0.2f;
        Material m = GetComponent<SpriteRenderer>().material;
        while (time < endTime)
        {
            m.SetFloat("_FlashAmount", Mathf.Lerp(0, 1, (endTime - time) / endTime));
            time += Time.deltaTime;
            yield return null;
        }
    }

    private void Die()
    {
        if (!dead)
        {
            dead = true;
            GameManager.Instance.Die();
        }
    }

    private void OnEnable()
    {
        SubmarineMovement.aboveWater += FillOxygen;
    }

    private void OnDisable()
    {
        SubmarineMovement.aboveWater -= FillOxygen;
    }
}