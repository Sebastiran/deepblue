﻿using UnityEngine;

public class Seller : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            if (!WindowManager.Instance.IsOpen((int)WindowManager.Window.Seller) && SubmarineMovement.water != (int)SubmarineMovement.Water.inWater)
            {
                GameManager.Instance.Pause = true;
                WindowManager.Instance.Open((int)WindowManager.Window.Seller);
            }
        }
    }
}
